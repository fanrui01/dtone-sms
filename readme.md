# dtone Coding Test

## Solution
This solution applies a typical 3-tier model, database - backend - frontend. db is an postgresql hosted on AWS RDS. Backend is a Django app hosted on AWS Lambda with Zappa framework. Frontend is a Rreact.js app hosted on a DigitalOcean server with help of docker and nginx.  

Database <====> Backend <====> Backend

## Database design - 3 tables
Implemented with Django ORM. 
#### Interest
- name (string)

#### Occupation
- name (string)

#### Record - the main table
- name (string)
- date-of-birth (date) (I use 'date of birth' instead of Age) 
- gender (string) - Select from 'M' and 'F'
- occupation (foreignkey) - A person can have only one occupation.
- interests (many2many, with a 'helper' table to store relations) - A person can have multiple interests.
- owner (foreignkey -> django's user (extened) ) - for bonus points

## Backend
Implemented with 'django rest framework' (DRF) and DRF-jwt. The backend can accept json web token (JWT) in Authorisation header, and therefore knows who is the user accessing the service. Permissions can be implemented according to user's identity, e.g., Admin user and non-Admin user can have different privileges over items. Due to time constraints, some unnecessary combinations of endpoint and request method are not blocked.
### Endpoints
#### Interest
~~~
/info/interests/
~~~
- POST, PATCH (with id), DELETE require admin user.
- GET requires no authentication


#### Occupation
~~~
/info/occupations/
~~~
- POST, PATCH (with id), DELETE require admin user.
- GET requires no authentication


#### Records
~~~
/info/records/
~~~
- POST, PATCH (with id), DELETE require logged-in user. A user can only access records created by himself. An admin user can access all records. (Here might be a bug I have not solved, saving by Admin user might cause change of ownership...)
- GET requires logged-in user. A user can only access records created by himself. An admin user can access all records.

## Front End
Due to time constraint, the frontend app only has the interaction functionality, while layout is not quite nicely considered. The front end is implemented in React.js with help of Redux (a series of libraries). Redux is a state control lib which helps the app to maintain state (here the auth info). APIs are implemented with js Promise. JWT token is added to api calls to validate the logged-in user's identity. The App has two pages (-- somehow to make it multiple pages, not really necessary), Home page and Record page. 

### Authentication
User can log in / out from the header. When not logged in, user cannot see records. When logged in as an Admin user, user can see/update/delete any records. When logged in as a non-Admin user, user can see/update/delete records belong to himself. All logged in users can create records.

### Frontend validation
Front end validation only applies to name and date of birth field. They must follow some regular expression. Warning will be given if input is not valid and Add/Edit actions will be blocked.

## Django Admin Page
[Admin Page - I did not customise the descriptions](https://j1banbbvsb.execute-api.ap-southeast-1.amazonaws.com/dev/admin)
### Superuser login - can access the Admin Page
~~~
email: admin@aaa.com
password: adminadmin
~~~

### Add Interests and Occupations in the Admin Page
It is better to add Interests and Occupations from the Admin Page since usually they are not for the users to add freely.

### Add users
As a superuser, a user can add another user. By default, a newly added user is not a super user. The '**LIMIT** of 5 users created' is not implemented. I could *imagine* that it requires a 'CreatedBy' field for the extended django user model and some constraint implemented at database level to achieve the requirement. 


## A Second Solution - All by django admin
I am sure that all of the above implemented functionality can be implemented with Django Admin withou using React.js. If necessary, we can have the further discussion later.

## Run locally
### Backend
~~~
python -m virtualenv venv
venv\Scripts\activate
cd project
python manage.py runserver --settings=project.settings.local
~~~

### Frontend
~~~
yarn start
~~~

## Deployment
### Backend 
[Admin Page](https://j1banbbvsb.execute-api.ap-southeast-1.amazonaws.com/dev/admin)
~~~
zappa deploy
~~~
AND
~~~
zappa update dev
~~~

### Frontend
[dtone Coding Test - Record Page](http://dtone.ricky89.com/record/)

Frontend project has a CircleCI pipeline implemented. It runs on master branch. 

## Source Control
[Front End Repo](https://bitbucket.org/fanrui01/dtone-frontend/src/master/)

[Back End Repo](https://bitbucket.org/fanrui01/dtone-backend/src/master/)

## Things missing
- Unit test cases.
- Credentials like DB connection are stored in text. They should be injected as environment variables in CI/CD pipeline.
- Frontend layout should be better designed and implemented.
- Still a lot to improve...




