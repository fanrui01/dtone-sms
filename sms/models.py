from django.db import models

# Create your models here.
#### Assumption:
# Each sender has only one number.
class Sender(models.Model):
    # name = models.CharField(max_length=100)
    user = models.ForeignKey('account.User', on_delete=models.DO_NOTHING)
    number = models.CharField(max_length=20)

    def __str__(self):
        return str(self.user.email)
    


class Message(models.Model):
    sender = models.ForeignKey('Sender', on_delete=models.DO_NOTHING)
    receiver_number = models.CharField(max_length=20)
    message = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.sender) + " to " + str(self.receiver_number) + ": " + str(self.message)


class MessageAttempt(models.Model):
    message = models.ForeignKey('Message', on_delete=models.DO_NOTHING)
    is_successful = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    telecom_partner = models.ForeignKey('TelecomPartner', on_delete=models.DO_NOTHING)
    delay_time = models.FloatField()

    def __str__(self):
        return str(self.message) + " @ " + str(self.created_at)


class TelecomPartner(models.Model):
    name = models.CharField(max_length=100)
    success_rate = models.FloatField()
    avg_delay_time = models.FloatField()

    def __str__(self):
        return str(self.name)
