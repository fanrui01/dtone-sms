from django.contrib import admin
from sms.models import Message, MessageAttempt, Sender, TelecomPartner
# Register your models here.

admin.site.register(Sender)
admin.site.register(Message)
admin.site.register(MessageAttempt)
admin.site.register(TelecomPartner)