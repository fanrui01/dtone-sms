from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import Sender, Message
from helperScripts.sqs_related import sqs_client, send as send_to_queue
import json

# Create your views here.
@csrf_exempt
def send_sms(request):
    if request.method == 'POST':
        user = request.user
        print("get_queryset: user: ", user)
        sender = Sender.objects.filter(user=user)[0]
        print("number: ", sender.number)
        body = json.loads(request.body)
        print("body: ", body)
        
        message = Message(
            sender=sender,
            receiver_number=body["receiver_number"],
            message=body["message"],
        )
        message.save()
        sms_obj = dict()
        sms_obj["from"] = sender.number
        sms_obj["to"] = body["receiver_number"]
        sms_obj["message"] = body["message"]
        sms_obj["message_instance_id"] = message.id
        sqs = sqs_client()
        message_id = send_to_queue(sqs, message_body=json.dumps(sms_obj) )

        response = dict()
        response["isSuccess"] = True
        response["sqs_message_id"] = message_id
        response["message_instance_id"] = message.id
        return JsonResponse(response)