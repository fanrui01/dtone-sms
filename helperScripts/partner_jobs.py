import sys
import os
import time
import json

import django

###############################################################################
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings.deploy')
django.setup()
###############################################################################
from constants import *

# from .sms import models
from sms.models import TelecomPartner, MessageAttempt

def analyse_partners(event=None, context=None):
    print("==== Get all historical attempts ====")
    telecom_partners = list(TelecomPartner.objects.all())

    
    for telecom_partner in telecom_partners:
        print("==== Calculate Success Rate and avg time spent ====")
        attempts = MessageAttempt.objects.filter(telecom_partner=telecom_partner)
        total_success = 0
        total_time_spent = 0
        for attempt in attempts:
            if attempt.is_successful == True:
                total_success += 1
            total_time_spent += attempt.delay_time
        success_rate = total_success / len(attempts)
        avg_time_spent = total_time_spent / len(attempts)
        telecom_partner.success_rate = success_rate
        telecom_partner.avg_delay_time=avg_time_spent
        telecom_partner.save()


if __name__ == "__main__":
    analyse_partners()


