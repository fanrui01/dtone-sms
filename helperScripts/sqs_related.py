import boto3
import constants

# Create SQS client
def sqs_client():
    sqs = boto3.client('sqs')
    return sqs

# attr_obj example
# MessageAttributes={
        #     'Title': {
        #         'DataType': 'String',
        #         'StringValue': 'The Whistler'
        #     },
        #     'Author': {
        #         'DataType': 'String',
        #         'StringValue': 'John Grisham'
        #     },
        #     'WeeksOn': {
        #         'DataType': 'Number',
        #         'StringValue': '6'
        #     }
        # },
# message_body example
# MessageBody=(
        #     'Information about current NY Times fiction bestseller for '
        #     'week of 12/11/2016.'
        # )

def send(sqs_client, attr_obj={}, message_body=''):
    # Send message to SQS queue
    response = sqs_client.send_message(
        QueueUrl=constants.SQS_QUEUE_URL,
        DelaySeconds=3,
        MessageAttributes=attr_obj,
        MessageBody=( message_body ),
    ) 

    print(response['MessageId'])
    message_id = response['MessageId']
    return  message_id


def receive_many(sqs_client, receive_number=1):
    response = sqs_client.receive_message(
        QueueUrl=constants.SQS_QUEUE_URL,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=receive_number,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=0,
        WaitTimeSeconds=0
    )
    # receipt_handles = list()
    messages = response['Messages']
    # for message in messages:
    # # messages = response['Messages'][0]
    #     receipt_handle = message['ReceiptHandle']
    # receipt_handles.append(receipt_handle)
    return messages


def delete_one(sqs_client, receipt_handle):
    # Delete received message from queue
    sqs_client.delete_message(
        QueueUrl=constants.SQS_QUEUE_URL,
        ReceiptHandle=receipt_handle
    )
    print('Received and deleted message: %s' % receipt_handle)
    
