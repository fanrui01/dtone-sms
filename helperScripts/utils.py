import random
import math

def random_pick_from_list(list_of_items):
    size = len(list_of_items)
    index_picked = math.floor(size * random.random())

    print("index_picked: ", index_picked)
    return list_of_items[index_picked]

