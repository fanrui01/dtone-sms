import requests
import datetime
import constants
import json

def send_sms(service_provider_name, obj):
    start_time = datetime.datetime.now()
    url = constants.SMS_URL_TEMPLATE.format(service_provider_name=service_provider_name)
    request = requests.post(url, data = obj)

    is_successful = json.loads(request.content)["isSuccessful"]

    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time).total_seconds()
    # print("response time: ", time_diff)
    return is_successful, time_diff



if __name__ == "__main__":
    send_sms("some_provider_name", {})
