import sys
import os
import time
import json

import django

###############################################################################
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings.deploy')
django.setup()
###############################################################################

from helperScripts.sms_related import send_sms
from helperScripts.sqs_related import sqs_client, receive_many, delete_one
from helperScripts.utils import random_pick_from_list

from constants import *

# from .sms import models
from sms.models import TelecomPartner, MessageAttempt, Message

def run_single_sms(event=None, context=None):
    sqs = sqs_client()
    print("==== Pick one from queue ====")
    try:
        message = receive_many(sqs, 1)[0]
        print("message: ", message)
        print("message['Body']:", message['Body'])
        print("json.loads(message['Body'])['from']: ", json.loads(message['Body'])['from'])
        message_body = json.loads(message['Body'])
        receipt_handle = message['ReceiptHandle']
        delete_one(sqs, receipt_handle)
    except Exception as e:
        print('Error pick from queue: ', e)
        return

    print("==== Select a telecom provider ====")
    telecom_partners = list(TelecomPartner.objects.all())
    try:
        selected_telecom_partner = random_pick_from_list(telecom_partners)
        print("selected_telecom_partner: ", selected_telecom_partner)
    except Exception as e:
        print('Error select telecom partner: ', e)
        return
    
    print("==== Send sms and log result ====")
    result, time_spent = send_sms(selected_telecom_partner.name, message_body)
    print("result: ", result)
    print("time_spent: ", time_spent)
    print('message_body["message_instance_id"]: ', message_body["message_instance_id"])
    message = Message.objects.filter(pk=message_body["message_instance_id"])[0]
    message_attempt = MessageAttempt(
        message = message,
        is_successful = result,
        telecom_partner = selected_telecom_partner,
        delay_time = time_spent,
    )
    message_attempt.save()



def run_single_sms_1(event=None, context=None):
    run_single_sms(event, context)

def run_single_sms_2(event=None, context=None):
    run_single_sms(event, context)

def run_single_sms_3(event=None, context=None):
    run_single_sms(event, context)

if __name__ == "__main__":
    run_single_sms()

