from rest_framework import permissions


class IsLoggedInUserOrAdmin(permissions.BasePermission):
    
    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user or request.user.is_staff

class IsLoggedInUser(permissions.BasePermission):
    def has_permission(self, request, view):
        print("request.user: ", request.user)
        print("request.user.is_authenticated: ", request.user.is_authenticated)
        print("request.user.is_staff: ", request.user.is_staff)
        if request.user.is_authenticated:
            return True
        return False

class IsAdminUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user and request.user.is_staff

    def has_object_permission(self, request, view, obj):
        return request.user and request.user.is_staff