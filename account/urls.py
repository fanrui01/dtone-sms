from django.urls import path, include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token
from account.views import UserViewSet

router = routers.DefaultRouter()
router.register(r'account', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
]

urlpatterns += [
    path('token-auth/', obtain_jwt_token),
    # the following to verify token
    path('token-verify/', verify_jwt_token)
]
