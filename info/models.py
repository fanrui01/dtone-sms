import datetime 
from django.db import models
from django.db.models.fields import CharField
from account.models import User

# Create your models here.
class Record(models.Model):
    MALE = 'M'
    FEMALE = 'F'
    GENDER_CHOICES = [
        (MALE, 'male'),
        (FEMALE, 'Female'),
    ]
    name = models.CharField(max_length=50)
    date_of_birth = models.DateField(default=datetime.date(1970, 1, 1))
    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES,
        default=MALE
    )
    occupation = models.ForeignKey('Occupation', on_delete=models.DO_NOTHING, null=True, blank=True)
    interests = models.ManyToManyField('Interest', null=True, blank=True)
    owner = models.ForeignKey('account.User', on_delete=models.DO_NOTHING, null=True, blank=True)

    def __str__(self):
        return self.name


class Occupation(models.Model):
    name = CharField(max_length=100)

    def __str__(self):
        return self.name

class Interest(models.Model):
    name = CharField(max_length=100) 

    def __str__(self):
        return self.name



