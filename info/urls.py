from django.urls import path, include
from rest_framework import routers
from .views import InterestViewSet, OccupationViewSet, RecordViewSet

interest_router = routers.DefaultRouter()
interest_router.register(r'', InterestViewSet)

occupation_router = routers.DefaultRouter()
occupation_router.register(r'', OccupationViewSet)

record_router = routers.DefaultRouter()
record_router.register(r'', RecordViewSet)

urlpatterns = [
    path('interests/', include(interest_router.urls)),
    path('occupations/', include(occupation_router.urls)),
    path('records/', include(record_router.urls)),
]
