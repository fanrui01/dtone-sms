from rest_framework import viewsets
from rest_framework.permissions import AllowAny

# from django.views.decorators.csrf import csrf_exempt, method_decorator

from .models import Interest, Occupation, Record
from .serializers import InterestSerializer, OccupationSerializer, RecordSerializer
from account.permissions import IsLoggedInUserOrAdmin, IsAdminUser, IsLoggedInUser

class InterestViewSet(viewsets.ModelViewSet):
    queryset = Interest.objects.all()
    serializer_class = InterestSerializer

    # Add this code block
    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [AllowAny]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUser]
        elif self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class OccupationViewSet(viewsets.ModelViewSet):
    queryset = Occupation.objects.all()
    serializer_class = OccupationSerializer

    # Add this code block
    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [AllowAny]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUser]
        elif self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

class RecordViewSet(viewsets.ModelViewSet):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer

    def perform_create(self, serializer):
        print("perform_create")
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        user = self.request.user
        print("get_queryset: user: ", user)
        print("get_queryset: user.is_staff: ", user.is_staff)
        if user.is_staff:
            return Record.objects.all()
        else:
            return Record.objects.filter(owner=user)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [IsLoggedInUser]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update':
            print("self.action: ", self.action)
            permission_classes = [IsLoggedInUser]
        elif self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]