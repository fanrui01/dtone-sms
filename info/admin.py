from django.contrib import admin
from .models import Occupation, Interest, Record
# Register your models here.
admin.site.register(Occupation)
admin.site.register(Interest)
admin.site.register(Record)